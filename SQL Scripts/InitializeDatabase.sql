CREATE DATABASE jonas_orders;

CREATE TABLE jonas_orders.orders (
	id integer not null auto_increment primary key ,
    distance integer not null,
    status enum("UNASSIGNED", "TAKEN") default 'UNASSIGNED'
);