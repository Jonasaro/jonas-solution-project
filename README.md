# Welcome to My Project!
> This is a simple back-end spring boot project about placing orders, taking orders, and getting an order list.

## Project Setup
* Build Tool Gradle 6.0
* MySQL Server 8.0
* Java jdk1.8.0_171
* Spring Boot 2.2.4
* Postman
* Spring JUnit 4 (When running JUnit Test Cases)
----
My apologies for not being able to integrate docker to my application. Because the Docker Desktop installer says *"Docker Desktop requires Windows 10 Pro or Enterprise version 15063 to run."*, and my computer doesn't run the said OS versions.

## Database Setup
1. Please run the **`InitializeDatabase.sql`** from the `SQL Scripts` folder under the root folder of the project, into your **MySQL Server/Workbench** to setup the database.
2. Set your database's **url**, **username**, and **password** in the `application.properties` file under the project's `src/main/resources `folder on **lines 6-8** for spring datasource's access.

>
    spring.datasource.driver-class-name=com.mysql.jdbc.Driver
    spring.datasource.url=jdbc:mysql://localhost:3306/jonas_orders?serverTimezone=UTC
    spring.datasource.username=root
    spring.datasource.password=admin

## Postman Request Collection
 As for sample requests, you can import the `Requests.postman_collection.json` file into your Postman so that you will have a copy of my requests Collection.


Thank you very much and have a great day!,
 **- Jonas Aro**