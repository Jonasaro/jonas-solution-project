package com.project.test.core.impl.usecase;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.project.app.constants.AppConstants;
import com.project.app.data.OrderDto;
import com.project.app.mapper.OrderMapper;
import com.project.app.repository.OrderRepository;
import com.project.app.responses.JonasSolutionResponse;
import com.project.app.responses.TakeOrderFailedResponse;
import com.project.app.responses.TakeOrderSuccessResponse;
import com.project.core.entities.OrderEntity;
import com.project.core.impl.usecase.MysqlSaveOrderDataImpl;
import com.project.test.data.TestData;

/**
 * Test class for the implementation of MySQL saving.
 * 
 * @author Jonas Aro
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class MysqlSaveOrderDataImplTest {

	@InjectMocks
	private MysqlSaveOrderDataImpl mysqlSaveOrderDataImpl = new MysqlSaveOrderDataImpl();

	@Mock
	private OrderRepository orderRepository;

	@Mock
	private OrderMapper orderMapper;

	TestData testData = new TestData();

	/**
	 * Tests the saveOrderStatusById of the MysqlSaveOrderDataImpl.
	 */
	@Test
	public void testSaveOrderStatusById() {
		Mockito.when(orderRepository.getOne(Mockito.anyInt())).thenReturn(testData.generateOrderEntity());
		Mockito.when(orderRepository.save(Mockito.any(OrderEntity.class))).thenReturn(testData.generateOrderEntity());

		JonasSolutionResponse jonasSolutionResponse = mysqlSaveOrderDataImpl
				.saveOrderStatusById(testData.generateTakenOrderDto(), 1);
		assertTrue(jonasSolutionResponse instanceof TakeOrderSuccessResponse);
	}

	/**
	 * Tests the saveOrderStatusById of the MysqlSaveOrderDataImpl with an already
	 * taken Order.
	 */
	@Test
	public void testSaveOrderStatusByIdOrderTaken() {
		OrderEntity orderEntity = testData.generateOrderEntity();
		orderEntity.setStatus(AppConstants.TAKEN);
		Mockito.when(orderRepository.getOne(Mockito.anyInt())).thenReturn(orderEntity);

		JonasSolutionResponse jonasSolutionResponse = mysqlSaveOrderDataImpl
				.saveOrderStatusById(testData.generateTakenOrderDto(), 1);
		assertTrue(jonasSolutionResponse instanceof TakeOrderFailedResponse);
	}

	/**
	 * Tests the saveOrderData method of the MysqlSaveOrderDataImpl.
	 */
	@Test
	public void testSaveOrderData() {
		OrderDto orderDto = testData.generateUnassignedOrderDto();
		OrderEntity orderEntity = testData.generateOrderEntity();
		Mockito.when(orderRepository.save(Mockito.any(OrderEntity.class))).thenReturn(orderEntity);
		Mockito.when(orderMapper.dtoToEntity(Mockito.any(OrderDto.class))).thenReturn(orderEntity);
		Mockito.when(orderMapper.entityToDto(Mockito.any(OrderEntity.class))).thenReturn(orderDto);
		OrderDto savedOrderDto = mysqlSaveOrderDataImpl.saveOrderData(orderDto);
		
		assertEquals(1, savedOrderDto.getId());
		assertEquals(1, savedOrderDto.getDistance());
		assertEquals(AppConstants.UNASSIGNED, savedOrderDto.getStatus());
	}

}
