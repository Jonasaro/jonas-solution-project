package com.project.test.core.impl.usecase;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.maps.errors.ApiException;
import com.project.app.requests.OrderRequest;
import com.project.core.impl.usecase.GetGoogleDistanceDataImpl;
import com.project.test.data.TestData;

/**
 * Test Class for getting Distance using Google Distance Matrix API.
 * 
 * A config class was created inside the test class and
 * used @ContextConfiguration in locating the config class to mock the property
 * file and @TestPropertySource was used in mocking a property source.
 * 
 * @author Jonas Aro
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { GetGoogleDistanceDataImplTest.Config.class })
@TestPropertySource(properties = { "googKey=AIzaSyB1kvAJG6g3PHdlgjk7fl8tLl1YZL1p7Ww" })
public class GetGoogleDistanceDataImplTest {

	@InjectMocks
	@Resource
	GetGoogleDistanceDataImpl getGoogleDistanceDataImpl = new GetGoogleDistanceDataImpl();

	public static class Config {
		@Bean
		GetGoogleDistanceDataImpl getMyClass() {
			return new GetGoogleDistanceDataImpl();
		}
	}

	/**
	 * Test the getDistanceUsingGoogle method of the GetGoogleDistanceDataImpl.
	 * 
	 * @throws ApiException         - when Google Distance Matrix Api Request fails
	 *                              awaiting.
	 * @throws InterruptedException - when Google Distance Matrix Api Request fails
	 *                              awaiting.
	 * @throws IOException          - when Google Distance Matrix Api Request fails
	 *                              awaiting.
	 */
	@Test
	public void testGetDistanceUsingGoogle() throws ApiException, InterruptedException, IOException {
		OrderRequest orderRequest = new TestData().generateOrderRequest();
		int distance = getGoogleDistanceDataImpl.getDistanceUsingGoogle(orderRequest);
		assertEquals(10407, distance);
	}
}
