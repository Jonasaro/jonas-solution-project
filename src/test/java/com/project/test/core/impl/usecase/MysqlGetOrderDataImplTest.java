package com.project.test.core.impl.usecase;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.project.app.constants.AppConstants;
import com.project.app.data.OrderDto;
import com.project.app.mapper.OrderMapper;
import com.project.app.repository.OrderRepository;
import com.project.core.entities.OrderEntity;
import com.project.core.impl.usecase.MysqlGetOrderDataImpl;
import com.project.test.data.TestData;

@RunWith(SpringJUnit4ClassRunner.class)
public class MysqlGetOrderDataImplTest {

	@InjectMocks
	MysqlGetOrderDataImpl mysqlGetOrderDataImpl = new MysqlGetOrderDataImpl();

	@Mock
	private OrderRepository orderRepository;

	@Mock
	private OrderMapper orderMapper;

	/**
	 * Tests the get order list method from the MysqlGetOrderDataImpl.
	 */
	@Test
	public void getOrderList() {
		List<OrderEntity> orderList = new TestData().generateOrderEntityList();
		Pageable orderPagable = PageRequest.of(0, 2);
		int start = (int) orderPagable.getOffset();
		int end = (start + orderPagable.getPageSize()) > orderList.size() ? orderList.size()
				: (start + orderPagable.getPageSize());
		Page<OrderEntity> orderPage = new PageImpl<OrderEntity>(orderList.subList(start, end), orderPagable,
				orderList.size());

		Mockito.when(orderRepository.findAll(Mockito.any(Pageable.class))).thenReturn(orderPage);
		Mockito.when(orderMapper.entityListToDtoList(Mockito.anyList()))
				.thenReturn(new TestData().generateOrderDtoList());

		List<OrderDto> orderDtoList = mysqlGetOrderDataImpl.getOrderList(1, 2);
		OrderDto firstOrderDto = orderDtoList.get(0);
		OrderDto secondOrderDto = orderDtoList.get(1);

		assertFalse(orderDtoList.isEmpty());

		assertEquals(1, firstOrderDto.getId());
		assertEquals(1, firstOrderDto.getDistance());
		assertEquals(AppConstants.UNASSIGNED, firstOrderDto.getStatus());

		assertEquals(2, secondOrderDto.getId());
		assertEquals(10, secondOrderDto.getDistance());
		assertEquals(AppConstants.UNASSIGNED, secondOrderDto.getStatus());
	}

	/**
	 * Tests the get order list method from the MysqlGetOrderDataImpl when the
	 * result is empty or the page has no content.
	 */
	@Test
	public void getOrderListEmptyPage() {
		List<OrderEntity> orderList = new ArrayList<OrderEntity>();
		Pageable orderPagable = PageRequest.of(0, 2);
		Page<OrderEntity> orderPage = new PageImpl<OrderEntity>(new ArrayList<OrderEntity>(), orderPagable,
				orderList.size());

		Mockito.when(orderRepository.findAll(Mockito.any(Pageable.class))).thenReturn(orderPage);
		Mockito.when(orderMapper.entityListToDtoList(Mockito.anyList())).thenReturn(new ArrayList<OrderDto>());

		List<OrderDto> orderDtoList = mysqlGetOrderDataImpl.getOrderList(1, 2);
		assertTrue(orderDtoList.isEmpty());
	}

	/**
	 * Tests the get order list method from the MysqlGetOrderDataImpl when page is 0
	 * or less.
	 */
	@Test
	public void getOrderListLessThanOrZeroPage() {
		List<OrderDto> orderDtoList = mysqlGetOrderDataImpl.getOrderList(0, 2);
		assertTrue(orderDtoList.isEmpty());
	}
}
