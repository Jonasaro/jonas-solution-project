package com.project.test.app.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.maps.errors.ApiException;
import com.project.app.constants.AppConstants;
import com.project.app.data.OrderDto;
import com.project.app.requests.OrderRequest;
import com.project.app.responses.JonasSolutionResponse;
import com.project.app.responses.PlaceOrderFailedResponse;
import com.project.app.responses.PlaceOrderSuccessResponse;
import com.project.app.responses.TakeOrderFailedResponse;
import com.project.app.responses.TakeOrderSuccessResponse;
import com.project.app.services.OrderService;
import com.project.core.usecases.GetDistanceDataUseCase;
import com.project.core.usecases.GetOrderDataUseCase;
import com.project.core.usecases.SaveOrderDataUsecase;
import com.project.test.data.TestData;

/**
 * Test Class for Order Service.
 * 
 * @author Jonas Aro
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class OrderServiceTest {

	@InjectMocks
	OrderService orderService = new OrderService();

	@Mock
	private GetDistanceDataUseCase getDistanceDataUseCase;

	@Mock
	private SaveOrderDataUsecase saveOrderDataUsecase;

	@Mock
	private GetOrderDataUseCase getOrderDataUseCase;

	/**
	 * Tests the place order method of the order service.
	 * 
	 * @throws ApiException         - when Google Distance Matrix Api Request fails
	 *                              awaiting.
	 * @throws InterruptedException - when Google Distance Matrix Api Request fails
	 *                              awaiting.
	 * @throws IOException          - when Google Distance Matrix Api Request fails
	 *                              awaiting.
	 */
	@Test
	public void testPlaceOrder() throws ApiException, InterruptedException, IOException {
		Mockito.when(getDistanceDataUseCase.getDistanceUsingGoogle(Mockito.any(OrderRequest.class))).thenReturn(1);
		Mockito.when(saveOrderDataUsecase.saveOrderDataInMySql(Mockito.any(OrderDto.class))).thenReturn(new OrderDto());
		JonasSolutionResponse jonasSolutionResponse = orderService.placeOrder(new OrderRequest());
		assertThat(jonasSolutionResponse instanceof PlaceOrderSuccessResponse);
	}

	/**
	 * Tests the place order method of the order service when an exception occurs.
	 * 
	 * @throws ApiException         - when Google Distance Matrix Api Request fails
	 *                              awaiting.
	 * @throws InterruptedException - when Google Distance Matrix Api Request fails
	 *                              awaiting.
	 * @throws IOException          - when Google Distance Matrix Api Request fails
	 *                              awaiting.
	 */
	@Test
	public void testPlaceOrderApiException() throws ApiException, InterruptedException, IOException {
		Mockito.when(getDistanceDataUseCase.getDistanceUsingGoogle(Mockito.any(OrderRequest.class)))
				.thenThrow(ApiException.class);
		Mockito.when(saveOrderDataUsecase.saveOrderDataInMySql(Mockito.any(OrderDto.class))).thenReturn(new OrderDto());
		JonasSolutionResponse jonasSolutionResponse = orderService.placeOrder(new OrderRequest());
		assertTrue(jonasSolutionResponse instanceof PlaceOrderFailedResponse);
	}

	/**
	 * Tests the take order method of the order service.
	 */
	@Test
	public void testTakeOrder() {
		Mockito.when(saveOrderDataUsecase.saveOrderStatusById(Mockito.any(OrderDto.class), Mockito.anyInt()))
				.thenReturn(new TakeOrderSuccessResponse(AppConstants.SUCCESS));
		JonasSolutionResponse jonasSolutionResponse = orderService.takeOrder(new OrderDto(), 1);
		assertTrue(jonasSolutionResponse instanceof TakeOrderSuccessResponse);
	}

	/**
	 * Tests the take order method of the order service when an entity was not found
	 * in the database.
	 */
	@Test
	public void testTakeOrderEntityNotFound() {
		Mockito.when(saveOrderDataUsecase.saveOrderStatusById(Mockito.any(OrderDto.class), Mockito.anyInt()))
				.thenThrow(EntityNotFoundException.class);
		JonasSolutionResponse jonasSolutionResponse = orderService.takeOrder(new OrderDto(), 1);
		assertTrue(jonasSolutionResponse instanceof TakeOrderFailedResponse);
	}

	/**
	 * Tests the get order list of the order service.
	 */
	@Test
	public void testGetOrderList() {
		Mockito.when(getOrderDataUseCase.getMysqlOrderList(Mockito.anyInt(), Mockito.anyInt()))
				.thenReturn(new TestData().generateOrderDtoList());

		List<OrderDto> orderDtoList = orderService.getOrderList(0, 2);
		OrderDto firstMockOrderDto = orderDtoList.get(0);
		OrderDto secondMockOrderDto = orderDtoList.get(1);

		assertFalse(orderDtoList.isEmpty());
		assertEquals(2, orderDtoList.size());
		assertEquals(1, firstMockOrderDto.getId());
		assertEquals(1, firstMockOrderDto.getDistance());
		assertTrue(AppConstants.UNASSIGNED.equals(firstMockOrderDto.getStatus()));

		assertEquals(2, secondMockOrderDto.getId());
		assertEquals(10, secondMockOrderDto.getDistance());
		assertTrue(AppConstants.UNASSIGNED.equals(secondMockOrderDto.getStatus()));
	}
	
}
