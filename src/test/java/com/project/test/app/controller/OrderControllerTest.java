package com.project.test.app.controller;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.project.app.constants.AppConstants;
import com.project.app.controller.OrderController;
import com.project.app.data.OrderDto;
import com.project.app.requests.OrderRequest;
import com.project.app.responses.GetOrderListErrorResponse;
import com.project.app.responses.JonasSolutionResponse;
import com.project.app.responses.PlaceOrderSuccessResponse;
import com.project.app.responses.TakeOrderSuccessResponse;
import com.project.app.services.OrderService;

/**
 * Test Class for Order Controller.
 * 
 * @author Jonas Aro
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class OrderControllerTest {

	@InjectMocks
	private OrderController orderController = new OrderController();

	@Mock
	private OrderService orderService;

	/**
	 * Tests the placeOrder request if it successfully calls the place Order in the
	 * Order Service.
	 */
	@Test
	public void testPlaceOrder() {
		Mockito.when(orderService.placeOrder(Mockito.any(OrderRequest.class)))
				.thenReturn(new PlaceOrderSuccessResponse(1, 1, AppConstants.UNASSIGNED));
		orderController.placeOrder(new OrderRequest());
		Mockito.verify(orderService).placeOrder(Mockito.any(OrderRequest.class));
	}

	/**
	 * Tests the placeOrder request if it successfully calls the take Order in the
	 * Order Service.
	 */
	@Test
	public void testTakeOrder() {
		Mockito.when(orderService.takeOrder(Mockito.any(OrderDto.class), Mockito.anyInt()))
				.thenReturn(new TakeOrderSuccessResponse(AppConstants.SUCCESS));
		orderController.takeOrder(new OrderDto(), 1);
		Mockito.verify(orderService).takeOrder(Mockito.any(OrderDto.class), Mockito.anyInt());
	}

	/**
	 * Tests the placeOrder request if it successfully calls the get Order list in
	 * the Order Service.
	 */
	@Test
	public void testOrderList() {
		Mockito.when(orderService.getOrderList(Mockito.anyInt(), Mockito.anyInt()))
				.thenReturn(new ArrayList<OrderDto>());
		orderController.getOrderList(1, 2);
		Mockito.verify(orderService).getOrderList(Mockito.anyInt(), Mockito.anyInt());
	}

	/**
	 * Tests the handling of missing parameters exception if ever there is a
	 * MethodArgumentTypeMismatchException from the controller, it would return an
	 * error response.
	 */
	@Test
	public void testHandleMissingParams() {
		JonasSolutionResponse jonasSolutionResponse = orderController.handleMissingParams(
				new MethodArgumentTypeMismatchException(new Object(), Integer.class, "Test", null, null));
		assertTrue(jonasSolutionResponse instanceof GetOrderListErrorResponse);
	}

	/**
	 * Tests the handling of number format exception if ever there is a
	 * MethodArgumentTypeMismatchException from the controller, it would return an
	 * error response.
	 */
	@Test
	public void testHandleNumberFormatParams() {
		JonasSolutionResponse jonasSolutionResponse = orderController
				.handleNumberFormatParams(new NumberFormatException());
		assertTrue(jonasSolutionResponse instanceof GetOrderListErrorResponse);
	}

}
