package com.project.test.data;

import java.util.ArrayList;
import java.util.List;

import com.project.app.constants.AppConstants;
import com.project.app.data.OrderDto;
import com.project.app.requests.OrderRequest;
import com.project.core.entities.OrderEntity;

/**
 * Test Data Class for Test Cases to use.
 * 
 * @author Jonas Aro
 */
public class TestData {
	
	/**
	 * Helper method in generating a list of order DTOs test data.. 
	 * 
	 * @return The generated list of order DTOs test data..
	 */
	public List<OrderDto> generateOrderDtoList() {
		List<OrderDto> mockOrderDtoList = new ArrayList<>();
		OrderDto firstMockOrderDto = new OrderDto(1, 1, AppConstants.UNASSIGNED);
		OrderDto secondMockOrderDto = new OrderDto(2, 10, AppConstants.UNASSIGNED);
		mockOrderDtoList.add(firstMockOrderDto);
		mockOrderDtoList.add(secondMockOrderDto);
		return mockOrderDtoList;
	}
	
	/**
	 * Helper method in generating a list of order entity test data.. 
	 * 
	 * @return The generated list of order entity test data..
	 */
	public List<OrderEntity> generateOrderEntityList() {
		List<OrderEntity> mockOrderEntityList = new ArrayList<>();
		OrderEntity firstMockOrderEntity = generateOrderEntity();
		OrderEntity secondMockOrderEntity = generateOrderEntity();
		secondMockOrderEntity.setId(2);
		secondMockOrderEntity.setDistance(10);
		mockOrderEntityList.add(firstMockOrderEntity);
		mockOrderEntityList.add(secondMockOrderEntity);
		
		return mockOrderEntityList;
	}
	
	/**
	 * Helper method in generating an order request test data..
	 * 
	 * @return The generated Order Request test data..
	 */
	public OrderRequest generateOrderRequest() {
		OrderRequest orderRequest = new OrderRequest();
		String[] origin = { "14.5907916", "121.0194582" };
		String[] destination = { "14.5359582", "121.0689909" };
		orderRequest.setOrigin(origin);
		orderRequest.setDestination(destination);
		return orderRequest;
	}
	
	/**
	 * Helper method in generating an order entity test data.
	 * 
	 * @return The generated order entity test data.
	 */
	public OrderEntity generateOrderEntity() {
		OrderEntity orderEntity = new OrderEntity();
		orderEntity.setId(1);
		orderEntity.setDistance(1);
		orderEntity.setStatus(AppConstants.UNASSIGNED);
		return orderEntity;
	}
	
	/**
	 * Helper method in generating a taken order DTO test data.
	 * 
	 * @return The generated order DTO test data.
	 */
	public OrderDto generateTakenOrderDto() {
		OrderDto orderDto = new OrderDto();
		orderDto.setId(1);
		orderDto.setDistance(1);
		orderDto.setStatus(AppConstants.TAKEN);
		return orderDto;
	}
	
	/**
	 * Helper method in generating an unassigned order DTO test data.
	 * 
	 * @return The generated order DTO test data.
	 */
	public OrderDto generateUnassignedOrderDto() {
		OrderDto orderDto = new OrderDto();
		orderDto.setId(1);
		orderDto.setDistance(1);
		orderDto.setStatus(AppConstants.UNASSIGNED);
		return orderDto;
	}
}
