package com.project.test.usecase;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.project.app.constants.AppConstants;
import com.project.app.data.OrderDto;
import com.project.app.responses.JonasSolutionResponse;
import com.project.app.responses.TakeOrderSuccessResponse;
import com.project.core.usecases.MysqlSaveOrderData;
import com.project.core.usecases.SaveOrderDataUsecase;
import com.project.test.data.TestData;

/**
 * Test Class for SaveOrderDataUsecase.
 * 
 * @author Jonas Aro
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class SaveOrderDataUsecaseTest {

	@InjectMocks
	private SaveOrderDataUsecase saveOrderDataUsecase;

	@Mock
	private MysqlSaveOrderData mysqlSaveOrderData;

	/**
	 * Tests the saveOrderStatusById method of SaveOrderDataUsecase.
	 */
	@Test
	public void testSaveOrderStatusById() {
		Mockito.when(mysqlSaveOrderData.saveOrderStatusById(Mockito.any(OrderDto.class), Mockito.anyInt()))
				.thenReturn(new TakeOrderSuccessResponse(AppConstants.SUCCESS));
		JonasSolutionResponse jonasSolutionResponse = saveOrderDataUsecase
				.saveOrderStatusById(new TestData().generateUnassignedOrderDto(), 1);

		Mockito.verify(mysqlSaveOrderData).saveOrderStatusById(Mockito.any(OrderDto.class), Mockito.anyInt());
		assertTrue(jonasSolutionResponse instanceof TakeOrderSuccessResponse);
	}

	/**
	 * Tests the saveOrderStatusById method of SaveOrderDataUsecase.
	 */
	@Test
	public void testSaveOrderDataInMySql() {
		Mockito.when(mysqlSaveOrderData.saveOrderData(Mockito.any(OrderDto.class)))
				.thenReturn(new TestData().generateTakenOrderDto());

		OrderDto orderDto = saveOrderDataUsecase.saveOrderDataInMySql(new TestData().generateUnassignedOrderDto());

		Mockito.verify(mysqlSaveOrderData).saveOrderData(Mockito.any(OrderDto.class));
		assertNotNull(orderDto);
		assertEquals(1, orderDto.getId());
		assertEquals(1, orderDto.getDistance());
	}

}
