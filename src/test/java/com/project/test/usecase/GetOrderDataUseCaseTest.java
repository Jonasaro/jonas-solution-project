package com.project.test.usecase;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.project.app.constants.AppConstants;
import com.project.app.data.OrderDto;
import com.project.core.usecases.GetOrderDataUseCase;
import com.project.core.usecases.MysqlGetOrderData;
import com.project.test.data.TestData;

/**
 * Test Class for GetOrderDataUseCase.
 * 
 * @author Jonas Aro
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class GetOrderDataUseCaseTest {
	
	@InjectMocks
	private GetOrderDataUseCase getOrderDataUseCase;

	@Mock
	private MysqlGetOrderData mysqlGetOrderData;

	/**
	 * Tests the getMysqlOrderList method of GetOrderDataUseCase.
	 */
	@Test
	public void testGetMysqlOrderList() {
		Mockito.when(mysqlGetOrderData.getOrderList(Mockito.anyInt(), Mockito.anyInt()))
				.thenReturn(new TestData().generateOrderDtoList());
		List<OrderDto> orderDtoList = getOrderDataUseCase.getMysqlOrderList(0, 1);
		OrderDto firstMockOrderDto = orderDtoList.get(0);
		OrderDto secondMockOrderDto = orderDtoList.get(1);
		
		assertFalse(orderDtoList.isEmpty());
		
		assertEquals(1, firstMockOrderDto.getId());
		assertEquals(1, firstMockOrderDto.getDistance());
		assertEquals(AppConstants.UNASSIGNED, firstMockOrderDto.getStatus());
		
		assertEquals(2, secondMockOrderDto.getId());
		assertEquals(10, secondMockOrderDto.getDistance());
		assertEquals(AppConstants.UNASSIGNED, secondMockOrderDto.getStatus());
	}
}
