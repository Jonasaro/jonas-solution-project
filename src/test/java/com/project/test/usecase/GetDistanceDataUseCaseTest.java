package com.project.test.usecase;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.maps.errors.ApiException;
import com.project.app.requests.OrderRequest;
import com.project.core.usecases.GetDistanceDataUseCase;
import com.project.core.usecases.GetGoogleDistanceData;
import com.project.test.data.TestData;

/**
 * Test Class for GetDistanceDataUseCase.
 * 
 * @author Jonas Aro
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class GetDistanceDataUseCaseTest {

	@InjectMocks
	private GetDistanceDataUseCase getDistanceDataUseCase;

	@Mock
	private GetGoogleDistanceData getGoogleDistanceData;

	/**
	 * Tests the getDistanceUsingGoogle method of GetDistanceDataUseCase
	 * 
	 * @throws ApiException         - when Google Distance Matrix Api Request fails
	 *                              awaiting.
	 * @throws InterruptedException - when Google Distance Matrix Api Request fails
	 *                              awaiting.
	 * @throws IOException          - when Google Distance Matrix Api Request fails
	 * 
	 */
	@Test
	public void testGetDistanceUsingGoogle() throws ApiException, InterruptedException, IOException {
		Mockito.when(getGoogleDistanceData.getDistanceUsingGoogle(Mockito.any(OrderRequest.class))).thenReturn(1);
		int distance = getDistanceDataUseCase.getDistanceUsingGoogle(new TestData().generateOrderRequest());
		assertEquals(1, distance);
	}
}
