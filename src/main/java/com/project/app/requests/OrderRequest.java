package com.project.app.requests;

/**
 * Request class for Order.
 * 
 * @author Jonas Aro
 */
public class OrderRequest {

	private String[] origin;
	private String[] destination;

	/**
	 * Getter for the origin coordinates from the Order Request.
	 * 
	 * @return The string array of coordinates of the Order.
	 */
	public String[] getOrigin() {
		return origin;
	}

	/**
	 * Setter for the origin coordinates from the Order Request.
	 * 
	 * @param origin - the string array of coordinates to be set into the origin of
	 *               the order.
	 */
	public void setOrigin(String[] origin) {
		this.origin = origin;
	}

	/**
	 * Getter for the destination from the Order Request.
	 * 
	 * @return The string array of the coordinates where the order is destined.
	 */
	public String[] getDestination() {
		return destination;
	}

	/**
	 * Setter for the destination coordinates of the order.
	 * 
	 * @param destination - the string array of coordinates to be set into the
	 *                    destination of the order.
	 */
	public void setDestination(String[] destination) {
		this.destination = destination;
	}

}
