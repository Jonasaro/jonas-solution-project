package com.project.app.services;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.maps.errors.ApiException;
import com.project.app.constants.AppConstants;
import com.project.app.data.OrderDto;
import com.project.app.requests.OrderRequest;
import com.project.app.responses.JonasSolutionResponse;
import com.project.app.responses.PlaceOrderFailedResponse;
import com.project.app.responses.PlaceOrderSuccessResponse;
import com.project.app.responses.TakeOrderFailedResponse;
import com.project.core.usecases.GetDistanceDataUseCase;
import com.project.core.usecases.GetOrderDataUseCase;
import com.project.core.usecases.SaveOrderDataUsecase;

/**
 * Service provider for Orders.
 * 
 * @author Jonas Aro
 */
@Service
public class OrderService {

	@Autowired
	private GetDistanceDataUseCase getDistanceDataUseCase;

	@Autowired
	private SaveOrderDataUsecase saveOrderDataUsecase;

	@Autowired
	private GetOrderDataUseCase getOrderDataUseCase;

	/**
	 * Service method for placing an order.
	 * 
	 * @param orderRequest - the order request that was placed.
	 * @return The response for the request.
	 */
	public JonasSolutionResponse placeOrder(OrderRequest orderRequest) {
		JonasSolutionResponse jonasSolutionResponse = null;
		try {
			OrderDto orderEntity = saveOrderDataUsecase.saveOrderDataInMySql(new OrderDto(0,
					getDistanceDataUseCase.getDistanceUsingGoogle(orderRequest), AppConstants.UNASSIGNED));

			jonasSolutionResponse = new PlaceOrderSuccessResponse(orderEntity.getId(), orderEntity.getDistance(),
					orderEntity.getStatus());
		} catch (ApiException | InterruptedException | IOException e) {
			jonasSolutionResponse = new PlaceOrderFailedResponse(e.getMessage());
		}
		return jonasSolutionResponse;
	}

	/**
	 * Service method for taking an order. Request Concurrency was handled by the
	 * server.tomcat.max-threads=1 in application.properties. This means that Every
	 * Entity can only have a single thread in saving.
	 * 
	 * @param orderDto - the DTO with the changes to be patched.
	 * @param id       - the ID to be used in referencing.
	 * @return The response for the request.
	 */
	public JonasSolutionResponse takeOrder(OrderDto orderDto, int id) {
		try {
			return saveOrderDataUsecase.saveOrderStatusById(orderDto, id);
		} catch (EntityNotFoundException e) {
			return new TakeOrderFailedResponse(AppConstants.ORDER_NOT_FOUND);
		}
	}

	/**
	 * Service method for getting the order list.
	 * 
	 * @param page  - the page to be viewed.
	 * @param limit - the page size or limit of data.
	 * @return The list of orders in the page.
	 */
	public List<OrderDto> getOrderList(int page, int limit) {
		return getOrderDataUseCase.getMysqlOrderList(page, limit);
	}
}
