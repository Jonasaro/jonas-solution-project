package com.project.app.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Main Class for my solution project.
 * 
 * @author Jonas Aro
 */

@SpringBootApplication
@ComponentScan("com.project")
@EnableJpaRepositories("com.project.app.repository")
@EntityScan("com.project.core.entities")
public class AppMain {

	/**
	 * Main Class of Jonas Solution Project.
	 * 
	 * @param args - the arguments to be passed into the main method.
	 */
	public static void main(String[] args) {
		SpringApplication.run(AppMain.class, args);
	}

}
