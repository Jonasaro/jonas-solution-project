package com.project.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.core.entities.OrderEntity;

/**
 * Repository for the orders.
 * 
 * @author Jonas Aro
 */
@Repository
public interface OrderRepository extends JpaRepository<OrderEntity, Integer> {
	
}
