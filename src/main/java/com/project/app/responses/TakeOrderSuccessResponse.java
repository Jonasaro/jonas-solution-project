package com.project.app.responses;

/**
 * Success Response for Taking of Order.
 * 
 * @author Jonas Aro
 */
public class TakeOrderSuccessResponse implements JonasSolutionResponse {

	private String status;
	
	/**
	 * Constructor for Take Order Success Response.
	 * 
	 * @param status - the status to be set into the Take Order Success Response.
	 */
	public TakeOrderSuccessResponse(String status) {
		this.status = status;
	}

	/**
	 * Getter for the Status from the Take Order Success Response.
	 * 
	 * @return The status from the Take Order Success Response.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Setter for the Status from the Take Order Success Response.
	 * 
	 * @param status - the status to be set into the Take Order Success Response.
	 */
	public void setStatus(String status) {
		this.status = status;
	}
}
