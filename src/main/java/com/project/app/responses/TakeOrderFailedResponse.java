package com.project.app.responses;

/**
 * Error/Failed Response for Taking of Order.
 * 
 * @author Jonas Aro
 */
public class TakeOrderFailedResponse implements JonasSolutionResponse {

	private String error;
	
	/**
	 * Constructor for the Take Order Error/Failed Response.
	 * 
	 * @param error - the error that will beset into the response.
	 */
	public TakeOrderFailedResponse(String error) {
		this.error = error;
	}

	/**
	 * Getter for the error of the Take Order Error/Failed Response.
	 * 
	 * @return The error from the Take Order Error/Failed Response.
	 */
	public String getError() {
		return error;
	}

	/**
	 * Setter for the error of the Take Order Error/Failed Response.
	 * 
	 * @param error - the error http status to be set into the Take Order
	 *              Error/Failed Response.
	 */
	public void setError(String error) {
		this.error = error;
	}

}
