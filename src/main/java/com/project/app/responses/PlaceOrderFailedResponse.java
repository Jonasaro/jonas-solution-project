package com.project.app.responses;

/**
 * Error/Failed Response for Place Order.
 * 
 * @author Jonas Aro
 */
public class PlaceOrderFailedResponse implements JonasSolutionResponse {

	private String error;
	
	public PlaceOrderFailedResponse(String error) {
		this.error = error;
	}

	/**
	 * Getter for the error of the Place Order Error/Failed Response.
	 * 
	 * @return The error from the Place Order Error/Failed Response.
	 */
	public String getError() {
		return error;
	}

	/**
	 * Setter for the error of the Place Order Error/Failed Response.
	 * 
	 * @param error - the error to be set into the Place Order
	 *              Error/Failed Response.
	 */
	public void setError(String error) {
		this.error = error;
	}
}
