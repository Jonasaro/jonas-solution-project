package com.project.app.responses;

/**
 * Success Response for Place Order.
 * 
 * @author Jonas Aro
 */
public class PlaceOrderSuccessResponse implements JonasSolutionResponse {

	private int id;
	private int distance;
	private String status;
	
	public PlaceOrderSuccessResponse(int id, int distance, String status) {
		this.id = id;
		this.distance = distance;
		this.status = status;
	}

	/**
	 * Getter for the ID from the Place Order Response.
	 * 
	 * @return The ID from the Place Order Response.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter for the ID for the ID in the Place Order Success Response.
	 * 
	 * @param id - the ID to be set into the Place Order Response.
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter for the Distance from the Place Order Success Response.
	 * 
	 * @return The distance from the Place Order Success Response.
	 */
	public int getDistance() {
		return distance;
	}

	/**
	 * Setter for the distance from the Place Order Success Response.
	 * 
	 * @param distance - the distance to be set into the Place Order Success
	 *                 Response.
	 */
	public void setDistance(int distance) {
		this.distance = distance;
	}

	/**
	 * Getter for the status from the Place Order Success Response.
	 * 
	 * @return The status from the Place Order Success Response.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Setter for the status from the Place Order Success Response.
	 * 
	 * @param status - the status to be set into the Place Order Success Response.
	 */
	public void setStatus(String status) {
		this.status = status;
	}
}
