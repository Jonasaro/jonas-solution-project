package com.project.app.responses;

/**
 * Error/Failed Response for Getting of Order List.
 * 
 * @author Jonas Aro
 *
 */
public class GetOrderListErrorResponse implements JonasSolutionResponse {

	private String error;
	
	/**
	 * Constructor for Getting of Order List.
	 * 
	 * @param error - the error to be set into the Getting of Order List Error/Failed Response.
	 */
	public GetOrderListErrorResponse(String error) {
		this.error = error;
	}

	/**
	 * Getter for the error of the Getting of Order List Error/Failed Response.
	 * 
	 * @return The error from the Getting of Order List Error/Failed Response.
	 */
	public String getError() {
		return error;
	}

	/**
	 * Setter for the error of the Getting of Order List Error/Failed Response.
	 * 
	 * @param error - the error to be set into the Getting of Order List
	 *              Error/Failed Response.
	 */
	public void setError(String error) {
		this.error = error;
	}
}
