package com.project.app.constants;

/**
 * My Application's Constants.
 * 
 * @author Jonas Aro
 */
public class AppConstants {
	
	/**
	 * Provided Google API Key.
	 */
	public static final String API_KEY = "${googKey}";
	
	/**
	 * English (United States) Language.
	 */
	public static final String EN_US_LANG = "en-US";
	
	/**
	 * Unassigned status for the Order.
	 */
	public static final String UNASSIGNED = "UNASSIGNED";
	
	/**
	 * Success status for taking Order.
	 */
	public static final String SUCCESS = "SUCCESS";
	
	/**
	 * Taken status for taking Order.
	 */
	public static final String TAKEN = "TAKEN";
	
	/**
	 * Order not found message.
	 */
	public static final String ORDER_NOT_FOUND = "Order Not Found!";
}
