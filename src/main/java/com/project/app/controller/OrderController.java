package com.project.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.project.app.data.OrderDto;
import com.project.app.requests.OrderRequest;
import com.project.app.responses.GetOrderListErrorResponse;
import com.project.app.responses.JonasSolutionResponse;
import com.project.app.services.OrderService;

/**
 * Controller for Order Requests.
 * 
 * @author Jonas Aro
 */
@RestController
public class OrderController {

	@Autowired
	private OrderService orderService;

	/**
	 * Request Mapping for Placing Orders.
	 * 
	 * @param orderRequest - the order request.
	 * @return The response to the request.
	 */
	@PostMapping("/orders")
	public JonasSolutionResponse placeOrder(@RequestBody(required = true) OrderRequest orderRequest) {
		return orderService.placeOrder(orderRequest);
	}

	/**
	 * Request Mapping for Patching Order updates.
	 * 
	 * @param orderDto - the order from the request body (request body has success
	 *                 property only).
	 * @param id       - the ID to be used in referencing what entity will be
	 *                 updated.
	 * @return The response to the request.
	 */
	@PatchMapping("/orders/{id}")
	public JonasSolutionResponse takeOrder(@RequestBody(required = true) OrderDto orderDto,
			@PathVariable("id") int id) {
		return orderService.takeOrder(orderDto, id);
	}

	/**
	 * Request Mapping for getting the order list in the desired page and page
	 * content limit.
	 * 
	 * @param page  - the page of orders to view.
	 * @param limit - the limit of orders to be viewed in the page.
	 * @return The response to the request.
	 */
	@GetMapping("/orders")
	public List<OrderDto> getOrderList(@RequestParam(required = true) int page, @RequestParam(required = true) int limit) {
		return orderService.getOrderList(page, limit);
	}

	/**
	 * Exception handler for missing parameters.
	 * 
	 * @param exception - when the controller throws an instance of
	 *                  MethodArgumentTypeMismatchException.
	 * @return The response to the request.
	 */
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public JonasSolutionResponse handleMissingParams(MethodArgumentTypeMismatchException exception) {
		return new GetOrderListErrorResponse(exception.getMessage());
	}
	
	/**
	 * Exception handler for character parameters instead of integer.
	 * 
	 * @param exception - when the controller throws an instance of
	 *                  NumberFormatException.
	 * @return The response to the request.
	 */
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(NumberFormatException.class)
	public JonasSolutionResponse handleNumberFormatParams(NumberFormatException exception) {
		return new GetOrderListErrorResponse(exception.getMessage());
	}
}
