package com.project.app.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.project.app.data.OrderDto;
import com.project.core.entities.OrderEntity;

/**
 * Mapper Class for Orders.
 * 
 * @author Jonas Aro
 */
@Component
public class OrderMapper {

	/**
	 * Mapper for Order DTO properties into an Order Entity properties.
	 * 
	 * @param orderDto - the order DTO with its property to be mapped into an order
	 *                 entity.
	 * @return The order entity with the mapped properties from the DTO.
	 */
	public OrderEntity dtoToEntity(OrderDto orderDto) {
		OrderEntity orderEntity = new OrderEntity();
		orderEntity.setId(orderDto.getId());
		orderEntity.setDistance(orderDto.getDistance());
		orderEntity.setStatus(orderDto.getStatus());

		return orderEntity;
	}

	/**
	 * Mapper for Order Entity properties into an Order DTO properties.
	 * 
	 * @param orderEntity - the order Entity with its property to be mapped into an
	 *                    order DTO.
	 * @return The order DTO with the mapped properties from the Entity.
	 */
	public OrderDto entityToDto(OrderEntity orderEntity) {
		OrderDto orderDto = new OrderDto();
		orderDto.setId(orderEntity.getId());
		orderDto.setDistance(orderEntity.getDistance());
		orderDto.setStatus(orderEntity.getStatus());

		return orderDto;
	}

	/**
	 * Mapper for Order DTO List properties into an Order Entity properties list.
	 * 
	 * @param orderDtoList - the order DTO list with its property to be mapped into
	 *                     an order entity list.
	 * @return The order entity list with the mapped properties from the DTO list.
	 */
	public List<OrderEntity> dtoListToEntityList(List<OrderDto> orderDtoList) {
		List<OrderEntity> orderEntityList = new ArrayList<>();
		orderDtoList.forEach(orderDto -> orderEntityList.add(dtoToEntity(orderDto)));
		return orderEntityList;
	}

	/**
	 * Mapper for Order Entity list properties into an Order DTO list properties.
	 * 
	 * @param orderEntityList - the order Entity list with its property to be mapped
	 *                        into an order DTO list.
	 * @return The order DTO list with the mapped properties from the Entity list.
	 */
	public List<OrderDto> entityListToDtoList(List<OrderEntity> orderEntityList) {
		List<OrderDto> orderDtoList = new ArrayList<>();
		orderEntityList.forEach(orderEntity -> orderDtoList.add(entityToDto(orderEntity)));
		return orderDtoList;
	}
}
