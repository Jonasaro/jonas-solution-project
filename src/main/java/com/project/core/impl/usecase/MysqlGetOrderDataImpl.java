package com.project.core.impl.usecase;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.project.app.data.OrderDto;
import com.project.app.mapper.OrderMapper;
import com.project.app.repository.OrderRepository;
import com.project.core.entities.OrderEntity;
import com.project.core.usecases.MysqlGetOrderData;

/**
 * Implementation for getting order data from MySQL.
 * 
 * @author Jonas Aro
 */
@Component
public class MysqlGetOrderDataImpl implements MysqlGetOrderData {

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private OrderMapper orderMapper;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<OrderDto> getOrderList(int page, int limit) {
		if (page > 0) {
			Pageable orderPage = PageRequest.of(page - 1, limit);
			Page<OrderEntity> orderPageResult = orderRepository.findAll(orderPage);

			if (orderPageResult.hasContent()) {
				return orderMapper.entityListToDtoList(orderPageResult.getContent());
			}
		}
		return new ArrayList<>();
	}

}
