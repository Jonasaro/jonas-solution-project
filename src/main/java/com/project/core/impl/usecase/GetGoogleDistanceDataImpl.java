package com.project.core.impl.usecase;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.DirectionsApi.RouteRestriction;
import com.google.maps.DistanceMatrixApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.LatLng;
import com.google.maps.model.TravelMode;
import com.project.app.constants.AppConstants;
import com.project.app.requests.OrderRequest;
import com.project.core.usecases.GetGoogleDistanceData;

/**
 * Implementation Class for Getting Distance Data
 * 
 * @author Jonas Aro
 */
@Component
public class GetGoogleDistanceDataImpl implements GetGoogleDistanceData {

	@Value(AppConstants.API_KEY)
	private String apiKey;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getDistanceUsingGoogle(OrderRequest orderRequest) throws ApiException, InterruptedException, IOException {
		GeoApiContext context = new GeoApiContext.Builder().apiKey(apiKey).build();
		DistanceMatrix distanceResult = null;
		DistanceMatrixApiRequest request = new DistanceMatrixApiRequest(context);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		LatLng originCoords = new LatLng(Double.valueOf(orderRequest.getOrigin()[0]),
				Double.valueOf(orderRequest.getOrigin()[1]));
		LatLng destinationCoords = new LatLng(Double.valueOf(orderRequest.getDestination()[0]),
				Double.valueOf(orderRequest.getDestination()[1]));

		distanceResult = request.origins(originCoords).destinations(destinationCoords).mode(TravelMode.DRIVING)
				.avoid(RouteRestriction.TOLLS).language(AppConstants.EN_US_LANG).await();

		return Integer.valueOf(gson.toJson(distanceResult.rows[0].elements[0].distance.inMeters));
	}

}
