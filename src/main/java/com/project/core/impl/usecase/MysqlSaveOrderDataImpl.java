package com.project.core.impl.usecase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.project.app.constants.AppConstants;
import com.project.app.data.OrderDto;
import com.project.app.mapper.OrderMapper;
import com.project.app.repository.OrderRepository;
import com.project.app.responses.JonasSolutionResponse;
import com.project.app.responses.TakeOrderFailedResponse;
import com.project.app.responses.TakeOrderSuccessResponse;
import com.project.core.entities.OrderEntity;
import com.project.core.usecases.MysqlSaveOrderData;

/**
 * Implementation for saving data into the MySQL Repository.
 * 
 * @author Jonas Aro
 */
@Component
public class MysqlSaveOrderDataImpl implements MysqlSaveOrderData {
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private OrderMapper orderMapper;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JonasSolutionResponse saveOrderStatusById(OrderDto orderDto, int id) {
		JonasSolutionResponse jonasSolutionResponse = null;
		OrderEntity orderEntity = orderRepository.getOne(id);
		if (AppConstants.UNASSIGNED.equalsIgnoreCase(orderEntity.getStatus())) {
			orderEntity.setStatus(orderDto.getStatus());
			orderRepository.save(orderEntity);
			jonasSolutionResponse = new TakeOrderSuccessResponse(AppConstants.SUCCESS);
		} else {
			jonasSolutionResponse = new TakeOrderFailedResponse("Order was already Taken!");
		}
		return jonasSolutionResponse;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public OrderDto saveOrderData(OrderDto orderDto) {
		return orderMapper.entityToDto(orderRepository.save(orderMapper.dtoToEntity(orderDto)));
	}

}
