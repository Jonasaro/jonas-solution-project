package com.project.core.usecases;

import java.util.List;

import org.springframework.stereotype.Component;

import com.project.app.data.OrderDto;

/**
 * Interface for getting order data from MySQL.
 * 
 * @author Jonas Aro
 */
@Component
public interface MysqlGetOrderData {
	
	/**
	 * Gets the order list with the provided page and limit.
	 * 
	 * @param page - the page to be viewed.
	 * @param limit - the page size or limit of data.
	 * @return The list of orders in the page.
	 */
	public List<OrderDto> getOrderList(int page, int limit);
}
