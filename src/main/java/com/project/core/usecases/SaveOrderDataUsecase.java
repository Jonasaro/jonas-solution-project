package com.project.core.usecases;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.project.app.data.OrderDto;
import com.project.app.responses.JonasSolutionResponse;

/**
 * Use Case for Saving Order Data.
 * 
 * @author Jonas Aro
 */
@Component
public class SaveOrderDataUsecase {

	@Autowired
	private MysqlSaveOrderData mysqlSaveOrderData;

	/**
	 * Save an order by using its ID into the MySQL database.
	 * 
	 * @param orderDto - the DTO to be patched into the orders database..
	 * @param id       - the ID to be referenced to what order will be saved.
	 * @return The saved order.
	 */
	public JonasSolutionResponse saveOrderStatusById(OrderDto orderDto, int id) {
		return mysqlSaveOrderData.saveOrderStatusById(orderDto, id);
	}

	/**
	 * Saves an order data into the MySQL database.
	 * 
	 * @param orderDto - the dto to be saved into the database.
	 * @return The saved order.
	 */
	public OrderDto saveOrderDataInMySql(OrderDto orderDto) {
		return mysqlSaveOrderData.saveOrderData(orderDto);
	}

}
