package com.project.core.usecases;

import java.io.IOException;

import org.springframework.stereotype.Component;

import com.google.maps.errors.ApiException;
import com.project.app.requests.OrderRequest;

/**
 * Interface of getting of Distance Data.
 * 
 * @author Jonas Aro
 */
@Component
public interface GetGoogleDistanceData {
	
	/**
	 * Method for getting the distance from the origin to the distance provided from
	 * the Order Request by Using the Google Distance Matrix API. 
	 * 
	 * @param orderRequest - the Order request with the needed origin and destination
	 *                 properties.
	 * @return The distance of the origin and destination in meters.
	 * 
	 * @throws ApiException - when Google Distance Matrix Api Request fails awaiting.
	 * @throws InterruptedException - when Google Distance Matrix Api Request fails awaiting.
	 * @throws IOException - when Google Distance Matrix Api Request fails awaiting.
	 */
	public int getDistanceUsingGoogle(OrderRequest orderRequest) throws ApiException, InterruptedException, IOException;
}
