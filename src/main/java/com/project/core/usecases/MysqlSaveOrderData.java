package com.project.core.usecases;

import org.springframework.stereotype.Component;

import com.project.app.data.OrderDto;
import com.project.app.responses.JonasSolutionResponse;

/**
 * Interface for saving Order data.
 * 
 * @author Jonas Aro
 */
@Component
public interface MysqlSaveOrderData {

	/**
	 * Save an order by using its ID.
	 * 
	 * @param orderDto - the DTO to be patched into the orders database..
	 * @param id       - the ID to be referenced to what order will be saved.
	 * @return The saved order.
	 */
	public JonasSolutionResponse saveOrderStatusById(OrderDto orderDto, int id);

	/**
	 * Saves an order data.
	 * 
	 * @param orderDto - the dto to be saved into the database.
	 * @return The saved order.
	 */
	public OrderDto saveOrderData(OrderDto orderDto);
}
