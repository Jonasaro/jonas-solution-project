package com.project.core.usecases;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.project.app.data.OrderDto;

/**
 * Use case for getting order data.
 * 
 * @author Jonas Aro
 */
@Component
public class GetOrderDataUseCase {
	
	@Autowired
	private MysqlGetOrderData mysqlGetOrderData;
	
	/**
	 * Gets the order list with the provided page and limit.
	 * 
	 * @param page - the page to be viewed.
	 * @param limit - the page size or limit of data.
	 * @return The list of orders in the page.
	 */
	public List<OrderDto> getMysqlOrderList(int page, int limit) {
		return mysqlGetOrderData.getOrderList(page, limit);
	}
}
