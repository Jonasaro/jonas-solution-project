package com.project.core.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity for an Order.
 * 
 * @author Jonas Aro
 */
@Entity
@Table(name = "orders")
public class OrderEntity {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "distance")
	private int distance;

	@Column(name = "status")
	private String status;

	/**
	 * Getter for the ID of the Order.
	 * 
	 * @return The ID of the order.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter for the ID of the Order.
	 * 
	 * @param id - the ID to be set into the Order.
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter for the distance of the Order.
	 * 
	 * @return The distance of the Order in meters.
	 */
	public int getDistance() {
		return distance;
	}

	/**
	 * Setter for the distance of the Order.
	 * 
	 * @param distance - the distance of the order.
	 */
	public void setDistance(int distance) {
		this.distance = distance;
	}

	/**
	 * Getter for the status of the order.
	 * 
	 * @return The status of the order.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Setter for the status of the order.
	 * 
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + distance;
		result = prime * result + id;
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderEntity other = (OrderEntity) obj;
		if (distance != other.distance)
			return false;
		if (id != other.id)
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}
}
